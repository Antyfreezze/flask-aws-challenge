import requests
from json import loads
from flask import Flask, request, jsonify, make_response


app = Flask(__name__)


@app.route('/smoke', methods=["GET"])
def smoke():
    return make_response("Hello world!")


@app.route("/performance", methods=["POST"])
def performance():
    urls_to_check = loads(request.data)["url"]
    request_time = 0
    for item in urls_to_check:
        time = requests.get(item).elapsed.total_seconds()
        request_time = request_time + time
    return make_response(
        jsonify({"message": f"Time to load = {request_time}"})
    )


if __name__ == "__main__":
    app.run()